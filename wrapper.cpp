#define _CRT_SECURE_NO_WARNINGS 1 // MSVS dno

#include <windows.h>
#include <string.h>
#include <stdio.h>

// settings
typedef char string256[256];

BOOL showConsole;

BOOL logEnabled;
string256 logFilename;

// log part
FILE *fLog, *fLog2;
CRITICAL_SECTION logCS;

void (__cdecl *real_Msg)(char *format, ...);

void __cdecl Msg(char *format, ...)
{
	char buffer[4096];
	
	EnterCriticalSection(&logCS);
	
	va_list v;
	va_start(v, format);
	vsprintf(buffer, format, v);
	va_end(v);
	
	fputs(buffer, fLog);
	fputc('\n', fLog);
	fflush(fLog);
	
	real_Msg(buffer);
	
	LeaveCriticalSection(&logCS);
}

// vfs part
void* (*vfs_ropen_os)(void *_this, const char *fn);
void* (*real_vfs_ropen_package)(void *_arg1, void *_arg2, const char *fn, int _arg3);

void* override_vfs_ropen_package(void *_arg1, void *_arg2, const char *fn, int _arg3)
{
	printf("%s\n", fn);
	if(fLog2) fprintf(fLog2, "%s\n", fn);
	
	if(GetFileAttributes(fn) != 0xFFFFFFFF)
	{
		return vfs_ropen_os(_arg1, fn);
	}
		
	return real_vfs_ropen_package(_arg1, _arg2, fn, _arg3);
}

// console part
void (*real_clevel_r_on_key_press)(void *_this, int key, int arg2, int arg3);

void **g_console;
void (*uconsole_server_impl_show)(void *_this);

void override_clevel_r_on_key_press(void *_this, int key, int arg2, int arg3)
{
	printf("key = %d, arg2 = %d, arg3 = %d\n", key, arg2, arg3);
	
	if(key == 39)
		uconsole_server_impl_show(*g_console);
	
	real_clevel_r_on_key_press(_this, key, arg2, arg3);
}

// walking fix attempt
extern "C" void walking_fix_attempt();

#if 0
__asm(
	".text\n"
	".global walking_fix_attempt\n"
	
	"walking_fix_attempt:\n"
	"movq 0x600(%rsi), %rax;\n"
	"testq %rax, %rax;\n"
	"jz notinitialized;\n"
	"xorl %r8d, %r8d;\n"
	"movl $0, %edx;\n"
	"movq 0x8(%rax), %rcx;\n"
	"movq (%rcx), %rax;\n"
	"call  *0x1E0(%rax);\n" // call mov_control_nx::activate_box
	"notinitialized:\n"
	// copied end of function cplayer::process_state
	"addq $0x100, %rsp;\n"
	"pop %rsi;\n"
	"pop %rbx;\n"
	"pop %rbp;\n"
	"ret;\n"
);
#endif

#if 1
__asm(
	".text\n"
	".global walking_fix_attempt\n"
	
	// rsi - pointer to player_new object
	// [rsi+0x600] - pointer to npc_physics

	"walking_fix_attempt:\n"
	"movq 0x600(%rsi), %rax;\n"
	"testq %rax, %rax;\n"          // if(player->_physics)
	"jz notinitialized;\n"
	"xorl %r8d, %r8d;\n"           
	"testl $0x40, 0x2B90(%rsi);\n" // if(!player->estate.crouch)
	"xorl %edx, %edx;\n"           // edx = 0;
	"setne %dl;\n"                 // else edx = 1;
	"movq 0x8(%rax), %rcx;\n"      // rcx = player->_physics->_mov_control
	"movq (%rcx), %rax;\n"         // rax = _mov_control vtable
	"call  *0x1E0(%rax);\n"        // call mov_control_nx::activate_box
	"notinitialized:\n"
	// copied end of function cplayer::process_state
	"addq $0x100, %rsp;\n"
	"pop %rsi;\n"
	"pop %rbx;\n"
	"pop %rbp;\n"
	"ret;\n"
);
#endif

BOOL PatchMem(LPVOID address, LPCVOID patchdata, int patchlen)
{
	DWORD oldProtection;
	BOOL ret;
	
	ret = VirtualProtect(address, 1, PAGE_EXECUTE_READWRITE, &oldProtection);
	if(ret)
	{
		memcpy(address, patchdata, patchlen);
		VirtualProtect(address, 1, oldProtection, &oldProtection);
		return TRUE;
	}
	
	return FALSE;
}

BOOL SetHook(LPVOID address, LPVOID detour)
{
#if defined(_M_IX86)

	#pragma pack(push,1)
    struct JMP
    {
        UINT8  mov_eax;
        UINT32 address;
        UINT16 jmp_eax;
    };
	#pragma pack(pop)

    CONST INT length = sizeof(JMP);

    DWORD oldProtection;
    if (VirtualProtect(address, length, PAGE_EXECUTE_READWRITE, &oldProtection) != TRUE)
    {
        return FALSE;
    }

    CONST JMP jmp =
    {
        0xB8,
        reinterpret_cast<UINT32>(detour),
        0xE0FF
    };

    memcpy(address, &jmp, length);

    VirtualProtect(address, length, oldProtection, &oldProtection);

    return TRUE;

#elif defined(_M_AMD64)

    #pragma pack(push,1)
    struct JMP
    {
        UINT16 jmp_to_pointer;
        UINT32 offset;
        UINT64 address;
    };
    #pragma pack(pop)

    struct JMP jmp =
    {
        0x25FF,
        0x00000000ul,
        0x0000000000000000ull
    };

	CONST INT length = sizeof(struct JMP);
   
    DWORD oldProtection;
    if (VirtualProtect(address, length, PAGE_EXECUTE_READWRITE, &oldProtection) != TRUE)
    {
        return FALSE;
    }

    jmp.address = (UINT64)detour;
    memcpy(address, &jmp, length);

    VirtualProtect(address, length, oldProtection, &oldProtection);

    return TRUE;

#else
	#error Unknown processor acrhitecture
#endif
}

LPVOID SetHookEx(LPVOID address, LPVOID detour, int preserve)
{
#if defined(_M_IX86)

    #pragma pack(push,1)
    struct JMP
    {
        UINT8  mov_eax;
        UINT32 address;
        UINT16 jmp_eax;
    };
    #pragma pack(pop)

    struct JMP jmp =
    {
        0xB8,
        0x00000000,
        0xE0FF
    };

    CONST INT length = sizeof(struct JMP);
    DWORD oldProtection;

    char *code;

    code = LocalAlloc(LMEM_FIXED, preserve + length);
    memcpy(code, address, preserve);

    jmp.address = (UINT32)address + preserve;
    memcpy(code + preserve, &jmp, length);

    if(VirtualProtect(code, preserve + length, PAGE_EXECUTE_READWRITE, &oldProtection) != TRUE)
    {
        free(code);
        return NULL;    
    }
    
    if(VirtualProtect(address, length, PAGE_EXECUTE_READWRITE, &oldProtection) != TRUE)
        return NULL;

    jmp.address = (UINT32)detour;
    memcpy(address, &jmp, length);

    VirtualProtect(address, length, oldProtection, &oldProtection);

    return code;

#elif defined(_M_AMD64)

    #pragma pack(push,1)
    struct JMP
    {
        UINT16 jmp_to_pointer;
        UINT32 offset;
        UINT64 address;
    };
    #pragma pack(pop)

    struct JMP jmp =
    {
        0x25FF,
        0x00000000ul,
        0x0000000000000000ull
    };

    CONST INT length = sizeof(struct JMP);
    DWORD oldProtection;
    
    char *code;

    code = (char*)LocalAlloc(LMEM_FIXED, preserve + length);
    memcpy(code, address, preserve);
    
    jmp.address = (UINT64)address + preserve;
    memcpy(code + preserve, &jmp, length);
    
    if(VirtualProtect(code, preserve + length, PAGE_EXECUTE_READWRITE, &oldProtection) != TRUE)
    {
        free(code);
        return NULL;    
    }
    
    if (VirtualProtect(address, length, PAGE_EXECUTE_READWRITE, &oldProtection) != TRUE)
        return NULL;

    jmp.address = (UINT64)detour;
    memcpy(address, &jmp, length);

    VirtualProtect(address, length, oldProtection, &oldProtection);

    return code;

#else
    #error Unknown processor acrhitecture
#endif
}

//
typedef HRESULT (WINAPI * LPFNDIRECTINPUT8CREATEPROC)(HINSTANCE, DWORD, REFIID, LPVOID, LPUNKNOWN);

HMODULE real_dinput8;
LPFNDIRECTINPUT8CREATEPROC real_DirectInput8Create;

#pragma comment(linker, "/export:DirectInput8Create@20=DirectInput8Create")
extern "C" __declspec(dllexport) HRESULT WINAPI DirectInput8Create(HINSTANCE hinst, DWORD dwversion, REFIID riidltf, LPVOID lpvout, LPUNKNOWN punkouter)
{
	if(!real_dinput8)
	{
		char path[512];
		GetSystemDirectory(path, sizeof(path));
		strcat(path, "\\dinput8.dll");

		real_dinput8 = LoadLibrary(path);
		real_DirectInput8Create = (LPFNDIRECTINPUT8CREATEPROC)GetProcAddress(real_dinput8, "DirectInput8Create");

		if(showConsole) {
			AllocConsole();
			freopen("CONOUT$", "w", stdout);
		}
	}

	return real_DirectInput8Create(hinst, dwversion, riidltf, lpvout, punkouter);
}

BOOL istrue(const char *str)
{
	return (strcmp(str, "true") == 0) || (strcmp(str, "yes") == 0) || (strcmp(str, "on") == 0) || (strcmp(str, "1") == 0);
}

BOOL DllMain(HINSTANCE hInstDLL, DWORD reason, LPVOID reserved)
{
	if(reason == DLL_PROCESS_ATTACH)
	{
		// load settings
		const char *cfg = ".\\developer.ini";
		string256 str;
		
		GetPrivateProfileString("console", "show", "false", str, sizeof(str), cfg);
		if(istrue(str))
			showConsole = TRUE;
		
		GetPrivateProfileString("log", "filename", "uengine.log", logFilename, sizeof(logFilename), cfg);
		GetPrivateProfileString("log", "enabled", "false", str, sizeof(str), cfg);
		
		// log hack
		if(istrue(str))
		{
			logEnabled = TRUE;
			
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				LPVOID original_log = LPBYTE(hArktika1) + 0x008CE190;
				
				InitializeCriticalSection(&logCS);
				fLog = fopen(logFilename, "w");
				
				LPVOID p = SetHookEx(original_log, LPVOID(Msg), 15);
				memcpy(&real_Msg, &p, sizeof(void*));
			}
		}
		
		// vfs hack
		{
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				LPVOID p1 = LPBYTE(hArktika1) + 0x008C6770; // vfs_ropen_os
				memcpy(&vfs_ropen_os, &p1, sizeof(void*));
			
				LPVOID p2 = LPBYTE(hArktika1) + 0x008C63D0; // vfs_ropen_package
				
				LPVOID code = SetHookEx(p2, LPVOID(override_vfs_ropen_package), 14);
    			memcpy(&real_vfs_ropen_package, &code, sizeof(void*));
			}
		}
		
		// console hack
		{
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				g_console = (void**)(LPBYTE(hArktika1) + 0x11C7D48);
				
				LPVOID p0 = LPBYTE(hArktika1) + 0x009254E0; // uconsole::server_impl::show
				memcpy(&uconsole_server_impl_show, &p0, sizeof(void*));
				
				LPVOID p1 = LPBYTE(hArktika1) + 0x0051D480; // clevel_r_on_key_press
				
				LPVOID code = SetHookEx(p1, LPVOID(override_clevel_r_on_key_press), 17);
    			memcpy(&real_clevel_r_on_key_press, &code, sizeof(void*));
			}
		}
		
		// walking hack
		// ������-�� ����������� ��������� ����������� ������ ��� ������������ ������ �������
		// �� ���� ����� � ���-��� ����, ������� ������ ��������� �������� activate_box ��� ������ �������
		// � �� ������ ��� �������/���������� ������ �������
		GetPrivateProfileString("arktika.1", "walking_hack", "false", str, sizeof(str), cfg);
		if(istrue(str))
		{
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				void *p = (LPBYTE(hArktika1) + 0x3A07C4); // end of function cplayer::process_state
				SetHook(p, LPVOID(walking_fix_attempt));
			}
		}
		
		// shaders hack
		// � �������.1 �� ��������� �� ���� ������ �������� ������������ /_OCULUS_=1, ������ ��� ������ ������� ������� �������� � ����
		// ������ ���� ��������� ���� � ���������� -build_key m3 �� /_OCULUS_=1 � ������ �������� �� ������������, � ������ ��������
		// �.�. �� ����� ����� � ����� ������ �������.
		// ����� ���� ��� ������������ ���� �������� ����� �������� ����� /_OCULUS_=1 ����������� ������, �� �������� �� �����
		GetPrivateProfileString("arktika.1", "shaders_hack", "false", str, sizeof(str), cfg);
		if(istrue(str))
		{
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				void *p = (LPBYTE(hArktika1) + 0xB4F99D);
				PatchMem(p, "\x90\x90\x90\x90\x90\x90", 6);
			}
		}
		
		// upgrades hack
		// ������-�� ��� ������ � 4� ������ �������� ������ � ����� ���������� ������� ����� ������
		// ������� �� � ��� ������ ������������ ����������� ���� ��� �������
		// ����� �� ���������, ����� ������� �� �������� ��� ������� � ������ -build_key m3
		GetPrivateProfileString("arktika.1", "upgrades_hack", "false", str, sizeof(str), cfg);
		if(istrue(str))
		{
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				// cplayer::load_dynamic, �������� �������
				// �������� ��������� ������ ������� ��� ��������� - ������ �������� �� ����� �� �����
				void *p1 = (LPBYTE(hArktika1) + 0x29F295);
				PatchMem(p1, "\xE9\xC6\x02\x00\x00\x90", 6);
				
				// ��������� ������ upgradables_registry_ovr ������ upgradables_registry
				// ����� ������� ��� �������� � config.bin, �� ��� ����
				void *p2 = (LPBYTE(hArktika1) + 0x5261EE);
				PatchMem(p2, "\x90\x90\x90\x90", 4);

				// ��� �����-�� ����� ���������� ��� ������ ������
				// ���� � ������ �� ���� ��� ����� ��� �������, ��� ��� ������ ���������
				void *p3 = (LPBYTE(hArktika1) + 0x31863C);
				PatchMem(p3, "\x0F\x95\xC0", 3);							
			}
		}
		
		// weapon hack
		// ��� � ����� ����� ���������� �� �� ���� ��� ����������� �������������� ������ �������� ��� ������
		// ��� ��� ��� ����� ����� � ����� ����������� ��� ������� ����������. �� ���-������ �����
		GetPrivateProfileString("arktika.1", "weapon_hack", "false", str, sizeof(str), cfg);
		if(istrue(str))
		{
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				void *p1 = (LPBYTE(hArktika1) + 0x3AFF31);
				PatchMem(p1, "\xE9\xC6\x00\x00\x00\x90", 6);							
			}
		}	
		
		// menu hack
		// ����� ��� �������� ����. ���� �� ���������� ������ ����������.
		GetPrivateProfileString("arktika.1", "menu_hack", "false", str, sizeof(str), cfg);
		if(istrue(str))
		{
			HMODULE hArktika1 = GetModuleHandle("arktika1.exe");
			if(hArktika1)
			{
				void *p1 = (LPBYTE(hArktika1) + 0x98EDAD);
				PatchMem(p1, "\xE9\x8D\x00\x00\x00\x90", 6);							
			}
		}						
	}
	
	return TRUE;
}
